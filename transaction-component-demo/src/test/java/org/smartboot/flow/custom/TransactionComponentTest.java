package org.smartboot.flow.custom;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.smartboot.flow.core.Feature;
import org.smartboot.flow.core.FlowEngine;
import org.smartboot.flow.core.SmartFlowConfiguration;
import org.smartboot.flow.core.manager.XmlGenerator;
import org.smartboot.flow.core.parse.ParseHelper;
import org.smartboot.flow.helper.view.PlantumlGenerator;

/**
 * @author qinluo
 * @date 2024-08-18 23:37:26
 * @since 1.0.0
 */
public class TransactionComponentTest {

    @BeforeEach
    public void setUp() {
        SmartFlowConfiguration.config(Feature.EnableInterceptFeature, Feature.values());
    }

    @Test
    public void testFirst() {

        FlowEngine<Object, Object> engine = ParseHelper.classpath("/transaction-component-first.xml")
                                            .first();

        engine.execute(1);

        XmlGenerator generator = new XmlGenerator();
        generator.generate(engine);

        System.out.println(generator.getContent());

        PlantumlGenerator plantumlGenerator = new PlantumlGenerator("transaction-component-first");
        plantumlGenerator.generate(engine);
    }
}
