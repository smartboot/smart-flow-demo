package org.smartboot.flow.custom;

import org.smartboot.flow.core.parse.ElementUtils;
import org.smartboot.flow.core.parse.ParserContext;
import org.smartboot.flow.core.parse.definition.ComponentDefinition;
import org.smartboot.flow.core.parse.parser.AbstractElementParser;
import org.smartboot.flow.core.util.AssertUtil;
import org.w3c.dom.Element;

import java.util.List;

/**
 * @author qinluo
 * @date 2024-08-18 23:12:57
 * @since 1.0.0
 */
public class TransactionComponentElementParser extends AbstractElementParser {

    @Override
    public String getElementName() {
        return "transaction-component";
    }

    @Override
    public void doParse(Element element, ParserContext context) {
        TransactionComponentDefinition def = new TransactionComponentDefinition();
        String id = getIdentifier(element, context);
        String transactionManager = element.getAttribute("transactionManager");
        AssertUtil.notBlank(transactionManager, "transactionManager cannot be blank");

        List<Element> subElements = ElementUtils.subElements(element);
        AssertUtil.isTrue(!subElements.isEmpty(), "transaction-component empty");

        ElementUtils.extraCommonAttributes(def, element);
        def.setIdentifier(id);
        ComponentDefinition comp = parseSubElements(element, subElements, context);
        def.setComponent(comp);

        def.getPropertyValues().put("transactionManager", transactionManager);
        def.getPropertyValues().put("component", comp.getIdentifier());
        def.getNonStandardAttributes().put("tag", getElementName());
        def.getNonStandardAttributes().put("prefix", element.getPrefix());
        context.register(def);
    }
}
