package org.smartboot.flow.custom;

/**
 * @author qinluo
 * @date 2024-08-18 23:29:04
 * @since 1.0.0
 */
public class TransactionManager {

    public <T> T run(TransactionExecutable<T> executable) {
        System.out.println("start transaction");
        T v = executable.execute();
        System.out.println("end transaction");
        return v;
    }

    public interface TransactionExecutable<T> {
        T execute();
    }
}
