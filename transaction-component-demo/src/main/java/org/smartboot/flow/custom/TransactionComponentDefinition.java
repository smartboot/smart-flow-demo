package org.smartboot.flow.custom;

import org.smartboot.flow.core.parse.DefinitionVisitor;
import org.smartboot.flow.core.parse.definition.ComponentDefinition;

/**
 * @author qinluo
 * @date 2024-08-18 23:11:48
 * @since 1.0.0
 */
public class TransactionComponentDefinition extends ComponentDefinition {

    private ComponentDefinition component;

    public ComponentDefinition getComponent() {
        return component;
    }

    public void setComponent(ComponentDefinition component) {
        this.component = component;
    }

    @Override
    public void doVisit(DefinitionVisitor visitor) {
        this.component.visit(visitor);
        visitor.visitCustomDefinition(this);
    }

    @Override
    public Class<?> resolveType() {
        return TransactionComponent.class;
    }
}
