package org.smartboot.flow.demo.executable;

import org.smartboot.flow.core.executable.AbstractExecutable;
import org.springframework.stereotype.Service;

/**
 * @author qinluo
 * @date 2023/2/11 19:16
 * @since 1.0.0
 */
@Service
public class ErrorStep extends AbstractExecutable<Integer, Integer> {

    @Override
    public void execute(Integer req, Integer result) {
        throw new RuntimeException("Error Step");
    }
}
