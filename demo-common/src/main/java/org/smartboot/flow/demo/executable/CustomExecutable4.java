package org.smartboot.flow.demo.executable;

import org.springframework.stereotype.Service;

/**
 * @author yamikaze
 * @date 2023/6/17 20:59
 * @since 1.0.0
 */
@Service
public class CustomExecutable4 {

    public static void execute(Integer request, Integer result, Exception customEx) {
        int a = 10;
        int b = 12;
        double d = 12;
        System.out.println("custom4 executed " + request + " result = " + result + " customExMsg = " + customEx.getMessage());

        System.out.println(a + b + d);
    }
}
