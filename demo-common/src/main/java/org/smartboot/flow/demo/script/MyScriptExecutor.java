package org.smartboot.flow.demo.script;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartboot.flow.core.EngineContext;
import org.smartboot.flow.core.script.ScriptExecutor;

/**
 * @author qinluo
 * @date 2023/2/11 19:34
 * @since 1.0.0
 */
public class MyScriptExecutor<T, S> extends ScriptExecutor<T, S> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyScriptExecutor.class);

    @Override
    public String getType() {
        return "custom";
    }

    @Override
    public Object execute(EngineContext<T, S> context) {
        LOGGER.info("script\n{}", this.script);
        return true;
    }
}
