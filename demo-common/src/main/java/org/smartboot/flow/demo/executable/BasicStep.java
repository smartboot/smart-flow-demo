package org.smartboot.flow.demo.executable;

import org.smartboot.flow.core.executable.AbstractExecutable;
import org.springframework.stereotype.Service;

/**
 * @author qinluo
 * @date 2024-08-18 21:38:45
 * @since 1.0.0
 */
@Service
public class BasicStep extends AbstractExecutable<Object, Object> {

    @Override
    public void execute(Object o, Object o2) {
        // Basic execute.
    }

    @Override
    public String describe() {
        return "basic";
    }
}
