package org.smartboot.flow.demo.executable;

import org.smartboot.flow.core.EngineContext;
import org.smartboot.flow.core.executable.AbstractExecutable;
import org.springframework.stereotype.Service;

/**
 * @author yamikaze
 * @date 2024/1/8 0:49
 * @since 1.0.0
 */
@Service
public class SleepStep extends AbstractExecutable<Integer, Integer> {

    private int mills;

    @Override
    public void execute(EngineContext<Integer, Integer> ctx) {
        long s = System.currentTimeMillis();
        try {
            if (mills > 0) {
                Thread.sleep(mills);
            }
        } catch (Exception e) {

        } finally {
            long e = System.currentTimeMillis();
            System.out.println("escape " + (e - s) + "ms");
        }
    }
}
