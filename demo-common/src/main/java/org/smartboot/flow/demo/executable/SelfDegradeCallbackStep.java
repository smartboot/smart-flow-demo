package org.smartboot.flow.demo.executable;

import org.smartboot.flow.core.DegradeCallback;
import org.smartboot.flow.core.EngineContext;
import org.smartboot.flow.core.executable.AbstractExecutable;
import org.springframework.stereotype.Service;

/**
 * @author qinluo
 * @date 2023/2/11 20:26
 * @since 1.0.0
 */
@Service
public class SelfDegradeCallbackStep extends AbstractExecutable<Object, Object> implements DegradeCallback<Object, Object> {

    @Override
    public void doWithDegrade(EngineContext<Object, Object> context, Throwable e) {
        System.out.println("self-degrade-callback invoked");
    }

    @Override
    public void execute(Object o, Object o2) {

    }
}
