package org.smartboot.flow.demo.executable;

import org.smartboot.plugin.executable.Param;
import org.springframework.stereotype.Service;

/**
 * @author yamikaze
 * @date 2023/6/17 20:59
 * @since 1.0.0
 */
@Service
public class CustomExecutable {

    public Object execute(@Param("req") Integer request, @Param("result") Integer result) {
        System.out.println("custom1 executed " + request + " result = " + result);
        return new Exception("hello, will passed by smart-flow engine.");
    }
}
