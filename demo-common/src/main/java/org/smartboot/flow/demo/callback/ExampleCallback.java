package org.smartboot.flow.demo.callback;

import org.smartboot.flow.core.DegradeCallback;
import org.smartboot.flow.core.EngineContext;
import org.springframework.stereotype.Service;

/**
 * @author qinluo
 * @date 2022-11-28 21:20:11
 * @since 1.0.0
 */
@Service
public class ExampleCallback implements DegradeCallback<Integer, String> {

    @Override
    public void doWithDegrade(EngineContext<Integer, String> context, Throwable e) {
        System.out.println("degrade callback called.");
    }

    @Override
    public String describe() {
        return "example-callback";
    }
}
