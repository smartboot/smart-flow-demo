package org.smartboot.flow.demo.executable;

import org.smartboot.plugin.executable.Param;
import org.springframework.stereotype.Service;

/**
 * @author yamikaze
 * @date 2023/6/17 20:59
 * @since 1.0.0
 */
@Service
public class CustomExecutable2 {

    public static void execute(@Param("req") Integer request, @Param("result") Integer result, Exception customEx) {
        int a = 10;
        int b = 12;
        double d = 12;
        System.out.println("custom2 executed " + request + " result = " + result + " customExMsg = " + customEx.getMessage());

        System.out.println(a + b + d);
    }
}
