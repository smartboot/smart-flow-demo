package org.smartboot.flow.demo.executable;

import org.smartboot.flow.core.exception.FlowException;
import org.smartboot.flow.core.executable.AbstractExecutable;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author qinluo
 * @date 2024-08-18 21:41:32
 * @since 1.0.0
 */
@Service
public class OddThrowExStep extends AbstractExecutable<Object, Object> {

    private final AtomicBoolean flag = new AtomicBoolean();

    @Override
    public void execute(Object o, Object o2) {
        if (flag.compareAndSet(false, true)) {
            throw new FlowException("OddThrowExStep fail");
        } else {
            flag.set(false);
        }
    }
}
