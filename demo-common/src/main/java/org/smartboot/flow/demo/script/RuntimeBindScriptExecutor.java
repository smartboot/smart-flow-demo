package org.smartboot.flow.demo.script;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartboot.flow.core.EngineContext;
import org.smartboot.flow.script.extension.qlexpress.QlExpressScriptExecutor;

import java.util.Map;

/**
 * @author qinluo
 * @date 2023/2/11 19:34
 * @since 1.0.0
 */
public class RuntimeBindScriptExecutor<T, S> extends QlExpressScriptExecutor<T, S> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RuntimeBindScriptExecutor.class);

    @Override
    public String getType() {
        return "runtimeBind";
    }

    @Override
    protected Map<String, Object> bindCustomized(EngineContext<T, S> context) {
        Map<String, Object> bound = super.bindCustomized(context);
        bound.put("runtime_variable", 1);

        return bound;
    }
}
