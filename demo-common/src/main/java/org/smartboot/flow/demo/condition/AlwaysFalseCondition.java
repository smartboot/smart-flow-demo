package org.smartboot.flow.demo.condition;


import org.smartboot.flow.core.Condition;
import org.smartboot.flow.core.EngineContext;
import org.springframework.stereotype.Service;

/**
 * @author qinluo
 * @date 2022-11-11 21:45:21
 * @since 1.0.0
 */
@Service
public class AlwaysFalseCondition extends Condition<Integer, Integer> {

    @Override
    public Object test(EngineContext<Integer, Integer> context) {
        return false;
    }

}
