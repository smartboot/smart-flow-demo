package org.smartboot.flow.demo.condition;


import org.smartboot.flow.core.Condition;
import org.springframework.stereotype.Service;

/**
 * @author qinluo
 * @date 2022-11-11 16:45:21
 * @since 1.0.0
 */
@Service
public class RequestChooseCondition extends Condition<Object, Object> {

    @Override
    public Object test(Object req, Object s) {
        return req;
    }
}
