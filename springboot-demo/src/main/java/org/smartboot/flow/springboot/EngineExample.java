package org.smartboot.flow.springboot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartboot.flow.core.EngineContext;
import org.smartboot.flow.core.FlowEngine;
import org.smartboot.flow.manager.reload.Reloader;
import org.smartboot.flow.spring.extension.ReloadNotify;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author qinluo
 * @date 2023-01-06 11:40:08
 * @since 1.0.0
 */
@Service
public class EngineExample implements InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(EngineExample.class);

    @Autowired
    @ReloadNotify("testEngine")
    private FlowEngine<Integer, String> testEngine;

    @ReloadNotify
    private Map<Integer, FlowEngine> testEngines;

    @Autowired
    Reloader reloader;

    public void execute() {
//        Object step = ctx.getBean("step3");
//        LOGGER.info("step is " + step.getClass().getName());

        //FlowEngine<Integer, String> testEngine = (FlowEngine<Integer, String>) ctx.getBean("testEngine", FlowEngine.class);

        EngineContext<Integer, String> executeContext = testEngine.execute(1);
        LOGGER.info("trace\n {}", executeContext.getTrace());
    }

    @Override
    public void afterPropertiesSet() throws Exception {

        //this.execute();

        new Thread(() -> {
            try {
                Thread.sleep(10000);
                execute();
            } catch (Exception ignore) {

            }
        }).start();

        //reloader.reload("testEngine");
    }
}
