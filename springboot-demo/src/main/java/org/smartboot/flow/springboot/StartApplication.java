package org.smartboot.flow.springboot;

import org.smartboot.flow.core.Feature;
import org.smartboot.flow.core.SmartFlowConfiguration;
import org.smartboot.flow.spring.extension.EasySmartFlow;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author qinluo
 * @date 2023-01-06 11:38:12
 * @since 1.0.0
 */

@EasySmartFlow
@SpringBootApplication(scanBasePackages =
        {
                "org.smartboot.flow.example",
                "org.smartboot.flow.springboot",
                "org.smartboot.flow.demo"
        })
public class StartApplication {

    public static void main(String[] args) throws Exception {
        SmartFlowConfiguration.config(Feature.RecordTrace, Feature.values());
        SpringApplication.run(StartApplication.class);
    }
}
