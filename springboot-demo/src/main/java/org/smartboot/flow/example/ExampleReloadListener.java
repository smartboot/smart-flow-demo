package org.smartboot.flow.example;

import org.smartboot.flow.manager.reload.ReloadListener;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

/**
 * @author qinluo
 * @date 2023-01-10 14:25:38
 * @since 1.0.0
 */
@Service
public class ExampleReloadListener implements ReloadListener, InitializingBean {

    @Override
    public void onload(String engineName) {
        System.out.println("start reload engine " + engineName);
    }

    @Override
    public void loadCompleted(String engineName, Throwable e) {
        if (e == null) {
            System.out.println("reload engine " + engineName + " success");
        } else {
            System.out.println("reload engine " + engineName + " failed " + e.getMessage());
        }
    }

    @Override
    public void afterPropertiesSet() {
        register();
    }
}
