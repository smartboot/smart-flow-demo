package org.smartboot.flow.example;


import org.smartboot.flow.core.Condition;
import org.springframework.stereotype.Service;

/**
 * @author qinluo
 * @date 2022-11-11 16:45:21
 * @since 1.0.0
 */
@Service
public class ChooseCondition extends Condition<Integer, String> {

    @Override
    public Object test(Integer integer, String s) {
        if (integer == null) {
            return "null";
        } else if (integer == 1) {
            return 1;
        } else if (integer == 2) {
            return 2;
        }
        return null;
    }
}
