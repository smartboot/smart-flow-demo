package org.smartboot.flow.example;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartboot.flow.core.Condition;
import org.smartboot.flow.core.EngineContext;
import org.smartboot.flow.core.Key;
import org.springframework.stereotype.Service;

/**
 * @author qinluo
 * @date 2022-11-11 21:45:21
 * @since 1.0.0
 */
@Service
public class ExecuteTimesCondition extends Condition<Integer, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecuteTimesCondition.class);

    @Override
    public Object test(Integer integer, String s) {
        return integer != null && integer == 1;
    }

    @Override
    public Object test(EngineContext<Integer, String> context) {
        Integer cnt = context.getExt(Key.of(this));
        if (cnt == null) {
            cnt = 3;
        }

        cnt--;
        context.putExt(Key.of(this), cnt);

        LOGGER.info("value {}", cnt);

        return cnt > 0;
    }
}
