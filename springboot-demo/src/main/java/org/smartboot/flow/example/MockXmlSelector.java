package org.smartboot.flow.example;

import org.smartboot.flow.manager.reload.XmlSelector;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * @author huqiang
 * @since 2022/12/7 19:54
 */
@Service("xxxxSelector")
@Primary
public class MockXmlSelector implements XmlSelector {

    @Override
    public String select(String engineName) {
        return "<engines>\n" +
                "    <engine code=\"testEngine\" name=\"testEngine\" pipeline=\"subprocess1\"/>\n" +
                "\n" +
                "    <pipeline name=\"subprocess9\" code=\"subprocess9\">\n" +
                "        <component execute=\"org.smartboot.flow.demo.executable.BasicStep\" name=\"step100\" rollback=\"true\"/>\n" +
                "        <component execute=\"org.smartboot.flow.demo.executable.BasicStep\" name=\"step102\"/>\n" +
                "    </pipeline>\n" +
                "\n" +
                "    <pipeline name=\"subprocess1\" code=\"subprocess1\">\n" +
                "        <component execute=\"org.smartboot.flow.demo.executable.BasicStep\" name=\"${aaa.bbb}\" rollback=\"true\"/>\n" +
                "        <component execute=\"selfDegradeCallbackStep\" name=\"step112\" degradable=\"true\"/>\n" +
                "        <component subprocess=\"subprocess9\"/>\n" +
                "        <pipeline name=\"subprocess2\" code=\"subprocess2\">\n" +
                "            <component execute=\"org.smartboot.flow.demo.executable.BasicStep\" name=\"step3\" rollback=\"true\"/>\n" +
                "            <component execute=\"org.smartboot.flow.demo.executable.BasicStep\" name=\"step4\"/>\n" +
                "        </pipeline>\n" +
                "\n" +
                "        <adapter execute=\"org.smartboot.flow.example.AdapterTest\" name=\"testAdapter\">\n" +
                "            <component execute=\"org.smartboot.flow.demo.executable.BasicStep\" name=\"newstep1\"/>\n" +
                "            <component execute=\"org.smartboot.flow.demo.executable.BasicStep\" name=\"newstep2\"/>\n" +
                "        </adapter>\n" +
                "        <if test=\"org.smartboot.flow.demo.condition.AlwaysTrueCondition\">\n" +
                "            <then>\n" +
                "                <pipeline name=\"subprocess3\" code=\"subprocess3\">\n" +
                "                    <if test=\"org.smartboot.flow.demo.condition.AlwaysFalseCondition\">\n" +
                "                        <then execute=\"org.smartboot.flow.demo.executable.BasicStep\" name=\"step7\"/>\n" +
                "                    </if>\n" +
                "                    <component execute=\"org.smartboot.flow.demo.executable.BasicStep\" name=\"step8\"/>\n" +
                "                </pipeline>\n" +
                "            </then>\n" +
                "            <else execute=\"org.smartboot.flow.demo.executable.BasicStep\" name=\"default\"/>\n" +
                "        </if>\n" +
                "        <choose test=\"chooseCondition\">\n" +
                "            <case when=\"1\">\n" +
                "                <component execute=\"org.smartboot.flow.demo.executable.BasicStep\" name=\"step9\"/>\n" +
                "                <component execute=\"org.smartboot.flow.demo.executable.BasicStep\" name=\"step10\"/>\n" +
                "            </case>\n" +
                "            <case when=\"null\" execute=\"org.smartboot.flow.demo.executable.BasicStep\" degradable=\"false\" degrade-callback=\"exampleCallback\"/>\n" +
                "            <default execute=\"org.smartboot.flow.demo.executable.BasicStep\"/>\n" +
                "        </choose>\n" +
                "\n" +
                "        <if test=\"alwaysFalseCondition\">\n" +
                "            <then>\n" +
                "                <component execute=\"org.smartboot.flow.demo.executable.BasicStep\" name=\"step5\"/>\n" +
                "                <component execute=\"org.smartboot.flow.demo.executable.BasicStep\" name=\"step6\"/>\n" +
                "            </then>\n" +
                "        </if>\n" +
                "    </pipeline>\n" +
                "\n" +
                "    <script name=\"alwaysFalseCondition\" type=\"javaScript\">\n" +
                "        var local = 2;\n" +
                "        local == request;\n" +
                "    </script>\n" +
                "\n" +
                "</engines>";
    }
}
