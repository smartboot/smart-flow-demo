package org.smartboot.flow.example;

import org.smartboot.flow.core.Adapter;
import org.smartboot.flow.core.EngineContext;
import org.smartboot.flow.core.common.Pair;

/**
 * @author huqiang
 * @since 2022/12/7 19:54
 */
public class AdapterTest implements Adapter<Integer, String, String, String> {

    @Override
    public Pair<String, String> before(EngineContext<Integer, String> context) {
        return Pair.of("req", "response");
    }

    @Override
    public void after(EngineContext<Integer, String> origin, EngineContext<String, String> newContext) {
        System.out.println("origin req = " + origin.getRequest() + ",  origin response = " + origin.getResult());
        System.out.println("req=" + newContext.getRequest() + ",  response=" + newContext.getResult());
    }

    @Override
    public String describe() {
        return this.getClass().getName();
    }
}
